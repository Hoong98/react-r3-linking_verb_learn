import  { useState,useRef,useEffect,forwardRef,useContext} from 'react';
import PreLoaderContext from '../context/preload_context';



export const Audio = forwardRef((props,ref) =>{
  const [show,setShow] = useState(false);
  let nextInterval      = null;


  useEffect(()=>{
    setTimeout(()=>{
      setShow(true)
    },props.delay);
  },[props.delay])

  function onPlaying(e) {
    if (props.next_time) {
      nextInterval = setInterval(()=>{
        if(e.target.currentTime >= props.next_time){
           props.shower();
           clearInterval(nextInterval);
        }
      },1000)
    }
  }

  useEffect(()=>{
    return ()=>{
      if (props.next_time) {
        clearInterval(nextInterval)
      }
    }
  },[])

  return show && <audio  preload={"true"} style={props.style} className={props.className} src={props.src} ref={ref} autoPlay={props.autoPlay} onEnded={props.onEnded} onPlaying={onPlaying} onPlay={props.onPlay} type="audio/mpeg"></audio>
})

export function Passer(props){
  const preContext        = useContext(PreLoaderContext);
  const [imgView,setImg]  = useState(props.imgView);



  useEffect(()=>{ setImg(props.imgView); },[props.imgView])
  const switchCall = () =>{
    if (!props.prevent && imgView != 0) {
      setImg(imgView == 1 ? 2:1);
    }
  }
  return  props.switcher ? <img src={props.switcher[imgView]} onClick={switchCall} className="absolute animated fadeIn pointer" style={{...style.top_center,...(props.prevent ? {left:'23%',width:'45%'}:{})}} />:null;
}


export const Video = forwardRef((props,ref) =>{
  const [show,setShow] = useState(false);
  let stopInterval = null;

  useEffect(()=>{
    setTimeout(()=>{
      setShow(true)
    },props.delay);
  },[props.delay])

  function onPlaying(e) {
    if (props.onPlaying) {
      props.onPlaying(e)
    }
    if (props.stop) {
      stopInterval = setInterval(()=>{
        if(e.target.currentTime >= props.stop){
          e.target.currentTime = props.stop;
          e.target.pause();
          clearInterval(stopInterval)
        }
      },1000)
    }
  }

  useEffect(()=>{
    return ()=>{
      if (props.stop) {
        clearInterval(setInterval)
      }
    }
  },[])

  return show && <video  preload={"true"}  onPlaying={onPlaying} style={props.style} className={props.className} src={props.src} ref={ref} autoPlay={props.autoPlay} loop={props.loop} onEnded={props.onEnded} onPlay={props.onPlay} type="audio/mpeg"></video>
})



const style = {
  top_center:{
    left:'18%',
    top:'7%',
    width:'50%'
  }
}
