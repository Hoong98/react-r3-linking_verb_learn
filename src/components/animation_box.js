import  { useState,useEffect,useContext,createRef } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';

//component
import {Video,Passer} from '../components/components'


export default function AnimationBox(props) {
  const preContext                        = useContext(PreLoaderContext);
  const [show,setShow]                    = useState(false);
  const [nextComponent,setNextComponent]  = useState(false);
  const [nextPath,setNext]                = useState();
  const nextImage                         = props.nextImage ? props.nextImage : preContext.images.next_button;
  const ref                               = createRef();
  let interval                            = '';
  let nextInterval                        = '';
  const [imgView,setImg]                  = useState(0);

  function videoEnd() {
    if (props.data.nextComponent) {
       setNextComponent(true);
    }else{
      if (!props.preventNext) {
        setShow(true);
      }
    }
  }

  const detectTime        = (e) => {
    if (!props.data.prevent) {
      interval = setInterval(()=>{
        if(e.target.currentTime >= props.data.secondImgeTime){
          setImg(1);
          clearInterval(interval);
        }
      },1000)
    }
    if (!props.preventNext) {
      nextInterval = setInterval(()=>{
        if(e.target.currentTime >= props.data.next_time){
          setShow(true);
          clearInterval(nextInterval)
        }
      },1000)
    }
  }
  useEffect(()=>{
    if (!props.singlePath) {
        if (props.index == props.maxIndex) {
          setNext(props.nextPath);
        }else{
            setNext(`${props.currentPath}/${props.index+1}`);
        }
    }else{
      setNext(props.nextPath);
    }
    return ()=>{
      if (!props.data.prevent && interval) {
        clearInterval(interval)
      }
      if (!props.preventNext) {
        clearInterval(nextInterval)
      }
    }
  },[])


  return nextComponent ? props.data.nextComponent : <>
              <Video ref={ref} stop={props.data.stop} src={props.data.video} onPlaying={detectTime} controls={true} style={style.full_video} preLoad={true} className="absolute" onEnded={videoEnd} delay={500} autoPlay={true} />
              <Passer imgView={imgView} prevent={props.data.prevent} switcher={props.data.switcher}/>
              {show && <Link to={nextPath}><img src={nextImage} className="center bottom-right animated fadeIn" style={{...style.next_button,...props.nextStyle}} /></Link>}</>
}

const style = {
  full_video:{
            width:'100%',
            left:0,
            textAlign:'center',
          },
   top_center:{
     left:'16%',
     top:'7%',
     width:'60%'
   },
   next_button:{
     width:'15%'
   }
}
