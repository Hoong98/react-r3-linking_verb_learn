
//<--------images-------->

//landing
import landing_back     from '../assets/img/landing/background.png';
import start_button     from '../assets/img/landing/start_button.png';

//intro
import intro_back       from '../assets/img/intro/background.png';
import text_img         from '../assets/img/intro/text.png';
import next_button      from '../assets/img/universal/next_button.png';

//menu
import menu_button       from '../assets/img/universal/menu_button.png';
import menu_button_back  from '../assets/img/universal/menu_button_base.png';
import menu_modal_back   from '../assets/img/menu/background.png';
import menu_modal_close  from '../assets/img/menu/x.png';
import intro_button         from '../assets/img/menu/Introduction_button.png';
import identify_button      from '../assets/img/menu/Identifying_button.png';
import linking_button       from '../assets/img/menu/Linking_verbs_button.png';
import using_button         from '../assets/img/menu/Using_Linking_Verbs_button.png';
import forming_button       from '../assets/img/menu/Forming_sentences_button.png';
import review1_button       from '../assets/img/menu/Review_1_button.png';
import more_button          from '../assets/img/menu/More_Linking_Verbs_button.png';
import using_more_button    from '../assets/img/menu/Using_more_Linking_Verbs.png';
import review2_button       from '../assets/img/menu/Review_2_button.png';

//universal
import image_first        from '../assets/img/universal/A_linking_verb_joins-with-arrow.png';
import image_second       from '../assets/img/universal/A_linking_verb_joins-with-arrow-short.png';

//linking_verbs
import linking_verbs_static_1       from '../assets/img/linking_verbs/A_linking_verb_joins.png'
import linking_verbs_static_2       from '../assets/img/linking_verbs/A_linking_verb_gives_more_information_about_a_thing.png'
import linking_verbs_first          from '../assets/img/linking_verbs/A_linking_verb_gives_more_information_about_a_thing-full-text-with-arrow.png';
import linking_verbs_second         from '../assets/img/linking_verbs/A_linking_verb_gives_more_information_about_a_thing-hidden-text-with-arrow.png';
import other_linking_verbs_include  from '../assets/img/linking_verbs/Other_linking_verbs_include.png';

//using_linking_verbs
import is         from '../assets/img/using_linking_verbs/is.png';
import has        from '../assets/img/using_linking_verbs/has.png';
import are        from '../assets/img/using_linking_verbs/are.png';
import have       from '../assets/img/using_linking_verbs/have.png';
import am         from '../assets/img/using_linking_verbs/am.png';
import all_verbs  from '../assets/img/using_linking_verbs/has.png';

//forming
import boy_cyling                   from '../assets/img/forming/boy-cyling.png';
import clock                        from '../assets/img/forming/clock.png';
import dirty_plate                  from '../assets/img/forming/dirty-plate.png';
import instrution_board             from '../assets/img/forming/instrution-board.png';
import man                          from '../assets/img/forming/man.png';
import swimming_pool                from '../assets/img/forming/swimming-pool.png';
import the_descriving_words_arrow   from '../assets/img/forming/The_descriving_words_can_be_an_action_full-text-with-arrow.png';
import the_descriving_words_hidden  from '../assets/img/forming/The_descriving_words_can_be_an_action_hidden-text-with-arrow.png';
import the_descriving_words_action  from '../assets/img/forming/The_descriving_words_can_be_an_action.png';

//review1
import review_am_is_has_are_have       from '../assets/img/review1/am_is_has_are_have.png';
import review_am_is_has                from '../assets/img/review1/am_is_has.png';
import background               from '../assets/img/review1/background.jpg';
import describing_words         from '../assets/img/review1/describing_words.png';
import play_button              from '../assets/img/review1/play_button.png';
import previous_button          from '../assets/img/review1/previous_button.png';
import wood_bar                 from '../assets/img/review1/wood_bar.png';
import x                        from '../assets/img/review1/x.png';

//more_linking_verbs
import more_linking_third         from '../assets/img/more_linking/other-linking-verbs-board-full-text-with-arrow.png';
import more_linking_first         from '../assets/img/more_linking/other-linking-verbs-board-full-text.png';
import more_linking_second        from '../assets/img/more_linking/other-linking-verbs-board-hidden-text-with-arrow.png';

//using_more_linking_verbs
import using_more_linking_verbs_first from '../assets/img/using_more_linking/which-linking-verbs-board.png'
import que_img_0 from '../assets/img/using_more_linking/animation/child_que/images/img_0.png';
import que_img_1 from '../assets/img/using_more_linking/animation/child_que/images/img_1.png';
import que_img_2 from '../assets/img/using_more_linking/animation/child_que/images/img_2.png';
import que_img_3 from '../assets/img/using_more_linking/animation/child_que/images/img_3.png';
import que_img_4 from '../assets/img/using_more_linking/animation/child_que/images/img_4.png';
import que_img_5 from '../assets/img/using_more_linking/animation/child_que/images/img_5.png';

// review2
import back_intro from '../assets/img/review2/back-to-introduction.png'

//<----audio file---->
import intro_audio     from '../assets/img/intro/vo/today_we_will_learn.mp3';

//review_1
import review_first_audio           from '../assets/img/review1/vo/Lets_review_what_we.mp3';
import the_describing_words_Track   from '../assets/img/review1/vo/The_describing_words_Track.mp3';
import so_we_use_I_Track            from '../assets/img/review1/vo/So_we_use_I_Track.mp3';
import linking_verb_is_Track        from '../assets/img/review1/vo/linking_verb_is_Track.mp3';

//using_more_linking_verbs
import using_more_linking_child_que_audio from '../assets/img/using_more_linking/vo/aud_que-v1.mp3';
import correct_answer_audio               from '../assets/img/using_more_linking/vo/09_V2_json2_Correct.mp3';
import wrong_answer_audio                 from '../assets/img/using_more_linking/vo/09_V2_json2_Wrong.mp3';
import next_click_audio                   from '../assets/img/using_more_linking/vo/click_next_audio.mp3'


//<-----video file---->
//intro
import intro_pre_audio from '../assets/img/intro/vo/1 Linking Verbs Learn_Track.mp3';

//identify
import identify_video from '../assets/img/identify/master1.mp4';

//liniking_verbs
import the_hat_is_big             from '../assets/img/linking_verbs/animation/01_the_hat_is_big-v3.mp4';
import the_hat_is_on_table        from '../assets/img/linking_verbs/animation/02_the_hat_is_on_table-v3.mp4';
import i_am_a_child               from '../assets/img/linking_verbs/animation/03_i_am_a_child-v3.mp4';
import you_are_tall               from '../assets/img/linking_verbs/animation/04_you_are_tall-v3.mp4';
import the_bird_are_flying        from '../assets/img/linking_verbs/animation/05_the_bird_are_flying-v3.mp4';
import the_zebra_has_stripes      from '../assets/img/linking_verbs/animation/06_the_zebra_has_stripes-v3.mp4';

//using_linking_verbs
import he_is_painting                 from '../assets/img/using_linking_verbs/animation/01_he_is_painting-v2.mp4';
import the_boy_has_paintbursh         from '../assets/img/using_linking_verbs/animation/02_the_boy_has_paintbursh-v2.mp4';
import they_are_at_the_playground     from '../assets/img/using_linking_verbs/animation/03_they_are_at_the_playground-v2.mp4';
import the_cat_have_long_tail         from '../assets/img/using_linking_verbs/animation/04_the_cat_have_long_tail-v2.mp4';
import i_am_hungry                    from '../assets/img/using_linking_verbs/animation/05_i_am_hungry-v2.mp4';
import am_is_has_are_have             from '../assets/img/using_linking_verbs/animation/06_am_is_has_are_have-v2.mp4'

//forming
import forming_bg_after   from '../assets/img/forming/animation/bg loop.mp4';
import forming_bg         from '../assets/img/forming/animation/Forming sentences with Linking Verbs.mp4'
import v_clock            from '../assets/img/forming/animation/desc/breakfast_is_at_eightoclock.mp4';
import v_swimming_pool    from '../assets/img/forming/animation/desc/i_am_at_the_swimming_pool.mp4';
import v_cycling          from '../assets/img/forming/animation/desc/i_am_cycling.mp4';
import v_dirty_plate      from '../assets/img/forming/animation/desc/the_dishes_are_dirty.mp4';
import v_man              from '../assets/img/forming/animation/desc/the_man_is_strong.mp4';

//more_linking_verbs
import more_linking_verbs_back from '../assets/img/more_linking/animation/Master.mp4'

//using_more_linking_verbs
import using_more_linking_verbs_01      from '../assets/img/using_more_linking/animation/painting.mp4'
import using_more_linking_verbs_loop    from '../assets/img/using_more_linking/animation/playground video loop.mp4'
import ice_cream                        from '../assets/img/using_more_linking/animation/ice_cream.mp4'

//review_2
import review2_back from '../assets/img/review2/animation/Master.mp4'

const img = {
    intro_back,
    start_button,
    text_img,
    next_button,
    landing_back,
    intro_button,
    identify_button,
    linking_button,
    using_button,
    forming_button,
    review1_button,
    more_button,
    using_more_button,
    review2_button,
    menu_button,
    menu_button_back,
    menu_modal_back,
    menu_modal_close,
    image_first,
    image_second,
    linking_verbs_first,
    linking_verbs_second,
    other_linking_verbs_include,
    all_verbs,
    is,
    are,
    am,
    have,
    has,
    boy_cyling,
    clock,
    dirty_plate,
    instrution_board,
    man,
    swimming_pool,
    the_descriving_words_arrow,
    the_descriving_words_hidden,
    the_descriving_words_action,
    review_am_is_has_are_have,
    review_am_is_has,
    background,
    describing_words,
    play_button,
    previous_button,
    wood_bar,
    x,
    linking_verbs_static_1,
    linking_verbs_static_2,
    more_linking_first,
    more_linking_second,
    more_linking_third,
    using_more_linking_verbs_first,
    que_img_0,
    que_img_1,
    que_img_2,
    que_img_3,
    que_img_4,
    que_img_5,
    back_intro
  }


const audio = {
    intro_pre_audio,
    intro_audio,
    using_more_linking_child_que_audio,
    correct_answer_audio,
    wrong_answer_audio,
    next_click_audio,
    review_first_audio,
    the_describing_words_Track,
    so_we_use_I_Track,
    linking_verb_is_Track
  }

const video={
    identify_video,
    the_hat_is_big,
    the_hat_is_on_table,
    i_am_a_child,
    you_are_tall,
    the_bird_are_flying,
    the_zebra_has_stripes,
    he_is_painting,
    the_boy_has_paintbursh,
    they_are_at_the_playground,
    the_cat_have_long_tail,
    i_am_hungry,
    am_is_has_are_have,
    v_clock,
    v_swimming_pool,
    v_cycling,
    v_dirty_plate,
    v_man,
    forming_bg,
    forming_bg_after,
    more_linking_verbs_back,
    using_more_linking_verbs_01,
    using_more_linking_verbs_loop,
    review2_back,
    ice_cream
  }


export {img,audio,video};
