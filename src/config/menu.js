
//pages
import Landing                      from '../pages/landing';
import Intro                        from '../pages/intro';
import Identify                     from '../pages/identify';
import LinkingVerbs                 from '../pages/linking_verbs';
import UsingLinkingVerbs            from '../pages/using_linking';
import Forming                      from '../pages/forming';
import Review1                      from '../pages/review1';
import MoreLinkingVerbs             from '../pages/more_linking';
import UsingMoreLinkingVerbs        from '../pages/using_more_linking';
import Review2                      from '../pages/review2';
import NotFound                     from '../pages/not_found';

//menus images
import {img} from './media';


const menu = [
  {
    path:'/',
    preventMenu:true,
    component:()=><Landing/>
  },
  {
    path:'/introduction',
    image:img.intro_button,
    direction:'left',
    top:5,
    component:()=><Intro/>
  },
  {
    path:'/identifying-linking-verbs-in-sentences',
    image:img.identify_button,
    direction:'left',
    top:19,
    component:()=><Identify/>
  },
  {
    path:'/linking_verbs/1',
    route_path:'/linking_verbs/:id',
    image:img.linking_button,
    direction:'left',
    top:33,
    component:(props)=><LinkingVerbs {...props}/>,
  },
  {
    path:'/using_linking_verbs/1',
    route_path:'/using_linking_verbs/:id',
    image:img.using_button,
    direction:'left',
    top:47,
    component:(props)=><UsingLinkingVerbs {...props}/>
  },
  {
    path:'/forming_sentences',
    image:img.forming_button,
    direction:'left',
    top:62,
    component:()=><Forming/>
  },
  {
    path:'/review_1',
    image:img.review1_button,
    direction:'right',
    top:10,
    component:()=><Review1/>
  },
  {
    path:'/more_linking_verbs',
    image:img.more_button,
    direction:'right',
    top:23,
    component:()=><MoreLinkingVerbs/>
  },
  {
    path:'/using_more_linking_verbs/1',
    route_path:'/using_more_linking_verbs/:id',
    image:img.using_more_button,
    direction:'right',
    top:36,
    component:(props)=><UsingMoreLinkingVerbs {...props}/>
  },
  {
    path:'/review_2',
    image:img.review2_button,
    direction:'right',
    top:49,
    component:()=><Review2/>
  },
];


export default menu;
