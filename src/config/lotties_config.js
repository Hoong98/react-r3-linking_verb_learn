//<---lottie json-->
//using_more_linking

import loading               from '../assets/lotties/loading.js';
import using_more_w          from '../assets/lotties/using_more_linking/wrong_feedback';
import using_more_r          from '../assets/lotties/using_more_linking/correct_feedback';
import using_more_child_que  from '../assets/lotties/using_more_linking/child_que';


const lotties={
  using_more_child_que,
  using_more_w,
  using_more_r,
  loading
}

export {lotties};
