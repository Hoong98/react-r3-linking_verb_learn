import  { useContext,useState,fragment,useEffect } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import {Audio} from '../components/components';




export default function Review1() {
  const preContext                 = useContext(PreLoaderContext);
  const [show,setShow]             = useState(false);
  const [modalValue,setModalValue] = useState();

  const arr            ={
    left  :[{
      image:preContext.images.review_am_is_has,
      audio:preContext.audios.linking_verb_is_Track
    },{
      image:preContext.images.review_am_is_has_are_have,
      audio:preContext.audios.so_we_use_I_Track
    }],
    right :[{
      image:preContext.images.describing_words,
      audio:preContext.audios.the_describing_words_Track
    }]
  }

  return <>
      <Audio shower={()=>setShow(true)} next_time={9} src={preContext.audios.review_first_audio} preLoad={true} delay={0} autoPlay={true} />
      <img src={preContext.images.background} className="center" style={style.landing_img}/>
      <img src={preContext.images.play_button}  onClick={()=>setModalValue('left')} className="absolute animated fadeIn pointer" style={style.play_button_left} />
      <img src={preContext.images.play_button}  onClick={()=>setModalValue('right')} className="absolute animated fadeIn pointer" style={style.play_button_right} />
      {modalValue && <Modal key={modalValue} onClose={()=>setModalValue("")} data={arr[modalValue]}/>}
      {show && <Link to='/more_linking_verbs'><img src={preContext.images.next_button} className="center animated fadeIn bottom-right" style={style.next_button} /></Link>}
  </>
}

function Modal(props) {
  const preContext            = useContext(PreLoaderContext);
  const [index,setIndex]      = useState(0);
  const [src,setSrc]          = useState();
  const [button,setButton]    = useState();

  return  <div className="absolute animated fadeIn"  style={style.modal}>
     <div style={style.modal_box} className="absolute" >
        <FragmentBox index={index} maxIndex={props.data.length-1} setIndex={setIndex} onClose={props.onClose}  src={props.data[index]}/>
     </div>
    </div>
}

function FragmentBox(props) {
    const preContext       = useContext(PreLoaderContext);
    const [close,setClose] = useState(false);
    const [back,setBack]   = useState(false);
    const [next,setNext]   = useState(false);

    useEffect(()=>{
      setClose(props.index == props.maxIndex)
      setBack(props.index != 0)
      setNext(props.index != props.maxIndex)
    },[props.index])
    return <>
                <Audio src={props.src.audio} preLoad={true} delay={0} autoPlay={true} />
                <img src={props.src.image} className="absolute" style={style.modal_back}/>
                {back && <img src={preContext.images.previous_button} onClick={()=>props.setIndex(props.index-1)} className="absolute pointer" style={style.modal_back_button}/>}
                {next && <img src={preContext.images.next_button} onClick={()=>props.setIndex(props.index+1)} className="absolute pointer" style={style.modal_next_button}/>}
                {close && <img src={preContext.images.x} style={style.close_button} onClick={props.onClose} className="absolute pointer"/>}
          </>
}

const style = {
  landing_img:{
            position:'absolute',
            width:'100%',
            left:0,
            top:0,
            textAlign:'center',
            zIndex:-1,
          },
   center_button:{
     width:'20%',
     bottom:'0%',
     zIndex:0
   },
   modal_back_button:{
     left: '-8%',
     width: '25%',
     bottom: '28%',
     top: 'initial'
   },
   modal_next_button:{
     left: 'initial',
     right:'-8%',
     width: '25%',
     bottom: '28%',
     top: 'initial'
   },
   play_button_left:{
     width: '7%',
     top: 'initial',
     bottom: '16%',
     left:'39%'
   },
   play_button_right:{
      width: '7%',
      top: 'initial',
      bottom: '16%',
      left:'83%'
    },
    modal_box:{
      width:'65%',
      right:0,
      top:'13%',
      paddingTop:'50%',
      margin:'auto',
      height:0
    },
    close_button:{
      right:'-1%',
      left:'initial',
      top:'-7%',
      width:'13%',
    },
    modal_back:{
     width:'100%'
    },
    modal:{
      width:'100%',
      height:'100%',
      margin:'auto',
      zIndex:9999
    },
    next_button:{
      width:'15%',
      right:'3%',
      bottom:'1%'
    }
}
