import  { useContext } from 'react';
import PreLoaderContext from '../context/preload_context';
import AnimationBox from '../components/animation_box';


export default function UsingLinkingVerbs(props){
  const preContext        = useContext(PreLoaderContext);
  const arr = [
    {},
    {
      switcher:[preContext.images.is],
      prevent:true,
      next_time:22,
      video:preContext.videos.he_is_painting
    },
    {
      switcher:[preContext.images.has],
      prevent:true,
      next_time:18,
      stop:20,
      video:preContext.videos.the_boy_has_paintbursh
    },
    {
      switcher:[preContext.images.are],
      prevent:true,
      next_time:24,
      video:preContext.videos.they_are_at_the_playground
    },
    {
      switcher:[preContext.images.have],
      prevent:true,
      next_time:23,
      video:preContext.videos.the_cat_have_long_tail
    },
    {
      switcher:[preContext.images.am],
      prevent:true,
      next_time:18,
      video:preContext.videos.i_am_hungry
    },
    {
      switcher:[preContext.images.linking_verbs_static_1,preContext.images.image_second,preContext.images.image_first],
      secondImgeTime:9,
      next_time:60,
      video:preContext.videos.am_is_has_are_have
    }
  ];

  return <AnimationBox
            key={props.match.params.id}
            nextPath="/forming_sentences"
            currentPath="/using_linking_verbs"
            index={parseInt(props.match.params.id)}
            maxIndex={arr.length-1}
            data={arr[parseInt(props.match.params.id)]}
            />;
}

// function UsingLinkingVerbsEvent(props) {
//   const preContext        = useContext(PreLoaderContext);
//   const [show,setShow]    = useState(false);
//   const [nextPath,setNext]= useState();
//   const ref               = createRef();
//   let interval            = '';
//
//   const [imgView,setImg]  = useState(0)
//
//   const detectTime        = (e) => {
//     if (!props.data.prevent) {
//       interval = setInterval(()=>{
//         if(e.target.currentTime >= props.data.secondImgeTime){
//           setImg(1);
//         }
//       },1000)
//     }
//   }
//   useEffect(()=>{
//     if (props.index == props.maxIndex) {
//       setNext('/forming_sentences');
//     }else{
//       setNext(`/using_linking_verbs/${props.index+1}`);
//     }
//     return ()=>{
//       if (!props.data.prevent && interval) {
//         clearInterval(interval)
//       }
//     }
//   },[])
//   return <>
//       <Video ref={ref} src={props.data.video} onPlaying={detectTime} controls={true} style={style.full_video} preLoad={true} className="absolute" onEnded={()=>setShow(true)} delay={500} autoPlay={true} />
//       <img src={props.data.switcher[imgView]} className="absolute animated fadeIn" style={{...style.top_center,...(props.data.prevent ? {left:'23%',width:'45%'}:{})}} />
//       {show && <Link to={nextPath}><img src={preContext.images.next_button} className="center bottom-right" style={style.next_button} /></Link>}
//   </>
// }
//
// const style = {
//   full_video:{
//             width:'100%',
//             left:0,
//             textAlign:'center',
//           },
//    top_center:{
//      left:'16%',
//      top:'7%',
//      width:'60%'
//    },
//    next_button:{
//      width:'15%'
//    }
// }
