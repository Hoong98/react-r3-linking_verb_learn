import  { useState } from 'react';
import { HashRouter as Router, Switch, Route } from "react-router-dom";
import menu from '../config/menu';
import LottieAnimation    from '../components/lottie';
import {lotties} from '../config/lotties_config';


//cache preload media
import PreLoader from '../components/preload_media.js';
import PreLoaderContext from '../context/preload_context';

//css
import '../assets/css/style.css';
import loading_icon from '../assets/img/universal/loading.gif';

//component
import Menu from './menu';

export default function Container() {
  const [loading,setLoading] = useState(true);
  const [images,setImages]     = useState();
  const [videos,setVideos]     = useState();
  const [audios,setAudios]     = useState();

  return  <>{loading ? <div className="full_center"><LottieAnimation data={lotties.loading} loop={true} style={style.lottie}/><PreLoader loading={setLoading} {...{setImages,setVideos,setAudios}}/></div> : <PreLoaderContext.Provider value={{images,videos,audios}} >
                <div className="container">
                <Router>
                  <Switch>
                  {menu.map(a=> <Route key={a.path} exact={true} path={(a.route_path ? a.route_path:a.path)} render={a.component}/>)}
                  </Switch>
                  <Menu/>
                </Router>
                </div>
              </PreLoaderContext.Provider>}</>
}


const style = {
   lottie:{
     top:'0%',
     width:'80%',
     margin:'auto',
     display:'block',
     left:'0%',
     right:'0%'
   },
}
