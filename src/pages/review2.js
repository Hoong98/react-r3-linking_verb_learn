import  { useState,useEffect,useContext,createRef } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import AnimationBox from '../components/animation_box';



export default function MoreLinkingVerbs(props){
  const preContext        = useContext(PreLoaderContext);
  const arr = [{},
    {
      switcher:[],
      video:preContext.videos.review2_back,
      next_time:17
    },
  ];

  return <AnimationBox nextStyle={{width:'40%',right:'1%',bottom:'1%'}}  nextImage={preContext.images.back_intro} singlePath={true} nextPath={"/"} data={arr[1]}/>;
}
