import  { useState,useContext,useRef} from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';



//component
import {Audio} from '../components/components'

export default function Intro() {
  const [start,setStart]          = useState(false);
  const [show,setShow]            = useState(false);
  const [nextAudio,setNextAudio]  = useState(false);
  const preContext     = useContext(PreLoaderContext);
  const audio = useRef();
  const nextAudioRef = useRef();

  return <>
      <Audio ref={audio} src={preContext.audios.intro_pre_audio}  onEnded={()=>{setNextAudio(true);nextAudioRef.current.play()}} autoPlay={false} />
      <Audio ref={nextAudioRef} src={preContext.audios.intro_audio} shower={()=> setShow(true)} next_time={10} onEnded={()=>setShow(true)} delay={0} autoPlay={false} />}
      <img src={preContext.images.intro_back} className="center" style={style.intro_img}/>
      {nextAudio && <img src={preContext.images.text_img} className="center animated fadeIn" style={style.center_button}/>}
      {!nextAudio && <img src={preContext.images.start_button} onClick={()=>audio.current.play()} className="center animated fadeIn active_button scale pointer" style={style.start_button}/>}
      {show && <Link to='/identifying-linking-verbs-in-sentences'><img src={preContext.images.next_button} className="center animated fadeIn bottom-right" style={style.next_button} /></Link>}
  </>
}



const style = {
  intro_img:{
            position:'absolute',
            width:'100%',
            left:0,
            top:0,
            textAlign:'center',
            zIndex:-1,
          },
   center_button:{
     width:'50%',
     bottom:'5%',
     zIndex:-1
   },
   start_button:{
     width:'20%',
     bottom:'0%',
     zIndex:0
   },
   next_button:{
     width:'15%'
   }
}
