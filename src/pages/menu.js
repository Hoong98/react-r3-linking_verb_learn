import  { useState,useContext } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';

//menu
import menu from '../config/menu';


export default function Menu(props) {
  const [menuShow,setMenuShow] = useState(false);
  const closeMenu              = ()=>setMenuShow(false);
  const preContext             = useContext(PreLoaderContext);

  return <div>
      <img src={preContext.images.menu_button_back} className="absolute" style={style.menu_button_back}/>
      <img src={preContext.images.menu_button} className="absolute pointer" onClick={()=>setMenuShow(true)}   style={style.menu_button}/>
      {menuShow && <div className="absolute animated fadeIn"  style={style.modal}>
         <div style={style.modal_menu} className="absolute" >
            <img src={preContext.images.menu_modal_back} style={style.modal_menu_back} className="absolute"/>
            <img src={preContext.images.menu_modal_close} style={style.close_button} onClick={closeMenu} className="absolute pointer"/>
            <div className="row absolute" >
              {menu.filter(a => !a.preventMenu).map((a,b)=>
                        <Link to={a.path} key={a.path} onClick={closeMenu} className={`${a.direction == 'left' ? 'img_left':'img_right'} absolute`}  style={{paddingTop:(a.top)+'%',zIndex:menu.length-b}}>
                          <img src={a.image} className="full-width"/>
                        </Link>
                      )}
            </div>
         </div>
        </div>}
  </div>;
}


const style={
  row_first:{
   paddingTop:'10%'
  },
  modal_menu:{
    width:'50%',
    right:0,
    top:'13%',
    paddingTop:'50%',
    margin:'auto',
    height:0
  },
  close_button:{
    right:'-7%',
    left:'initial',
    top:'-11%',
    width:'10%',
  },
  modal_menu_back:{
    width:'100%'
  },
  modal:{
    width:'100%',
    height:'100%',
    margin:'auto',
    zIndex:9999
  },
  menu_button_back:{
   width:'20%'
  },
  menu_button:{
    top:'5%',
    left:'3%',
    width:'6%'
  },
}
