import  { useContext } from 'react';
import PreLoaderContext from '../context/preload_context';
import AnimationBox from '../components/animation_box';


export default function LinkingVerbs(props){
  const preContext        = useContext(PreLoaderContext);
  const arr = [
    {},
    {
      switcher:[preContext.images.linking_verbs_static_1,preContext.images.image_second,preContext.images.image_first],
      secondImgeTime:10,
      next_time:42,
      video:preContext.videos.the_hat_is_big
    },
    {
      switcher:[preContext.images.linking_verbs_static_2,preContext.images.linking_verbs_second,preContext.images.linking_verbs_first],
      secondImgeTime:13,
      next_time:43,
      video:preContext.videos.the_hat_is_on_table
    },
    {
      switcher:[preContext.images.other_linking_verbs_include],
      prevent:true,
      next_time:27,
      video:preContext.videos.i_am_a_child
    },
    {
      switcher:[preContext.images.other_linking_verbs_include],
      prevent:true,
      next_time:17,
      video:preContext.videos.you_are_tall
    },
    {
      switcher:[preContext.images.other_linking_verbs_include],
      prevent:true,
      next_time:18,
      video:preContext.videos.the_bird_are_flying
    },
    {
      switcher:[preContext.images.other_linking_verbs_include],
      prevent:true,
      next_time:19,
      video:preContext.videos.the_zebra_has_stripes
    }
  ];

  return <AnimationBox
            key={props.match.params.id}
            nextPath="/using_linking_verbs/1"
            currentPath="/linking_verbs"
            index={parseInt(props.match.params.id)}
            maxIndex={arr.length-1}
            data={arr[parseInt(props.match.params.id)]}
            />;
}
