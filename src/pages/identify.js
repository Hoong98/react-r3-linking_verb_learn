import  { useContext } from 'react';
import PreLoaderContext from '../context/preload_context';
import AnimationBox from '../components/animation_box';


export default function Identify(props){
  const preContext        = useContext(PreLoaderContext);
  const arr = [{},
    {
      switcher:[preContext.images.image_first,preContext.images.image_second,preContext.images.image_first],
      secondImgeTime:9,
      next_time:41,
      video:preContext.videos.identify_video
    },
  ];
  return <AnimationBox nextPath="/linking_verbs/1" singlePath={true} data={arr[1]}/>;
}
