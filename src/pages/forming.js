import  { useState,useEffect,useContext,createRef,useMemo} from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import AnimationBox from '../components/animation_box';

//component
import {Video,Passer} from '../components/components'


export default function Forming(props){
  const preContext        = useContext(PreLoaderContext);
  const switcher          =[
                             preContext.images.the_descriving_words_action,
                             preContext.images.the_descriving_words_hidden,
                             preContext.images.the_descriving_words_arrow
                           ]
  const arr = [{},
    {
      switcher,
      secondImgeTime:13,
      next_time:24,
      video:preContext.videos.forming_bg,
      nextComponent:<FormingDesc switcher={switcher} />
    },
  ];

  return <AnimationBox currentPath="/linking_verbs" nextPath={'/review_1'}  singlePath={true} data={arr[1]}/>;
}



function FormingDesc(props) {
  const preContext        = useContext(PreLoaderContext);
  const [video,setVideo]  = useState(false);
  const videosList        = [
                              preContext.videos.v_clock,
                              preContext.videos.v_cycling,
                              preContext.videos.v_swimming_pool,
                              preContext.videos.v_man,
                              preContext.videos.v_dirty_plate,
                            ];
  return <>
        <Video src={preContext.videos.forming_bg_after} style={style.full_video} preLoad={true} className="absolute" delay={0} autoPlay={true} loop={true} />
        <img src={preContext.images.instrution_board}  className="absolute animated fadeIn" style={style.instrution_board} />
        <img src={preContext.images.clock}  onClick={()=>setVideo(videosList[0])} className="absolute animated fadeIn pointer hover_scale" style={style.clock} />
        <img src={preContext.images.boy_cyling} onClick={()=>setVideo(videosList[1])} className="absolute animated fadeIn pointer hover_scale" style={style.boy_cyling} />
        <img src={preContext.images.swimming_pool} onClick={()=>setVideo(videosList[2])} className="absolute animated fadeIn pointer hover_scale" style={style.swimming_pool} />
        <img src={preContext.images.man} onClick={()=>setVideo(videosList[3])} className="absolute animated fadeIn pointer hover_scale" style={style.man} />
        <img src={preContext.images.dirty_plate} onClick={()=>setVideo(videosList[4])} className="absolute animated fadeIn pointer hover_scale" style={style.plate} />
        <Link to="/review_1"><img src={preContext.images.next_button} className="center bottom-right cursor" style={style.next_button} /></Link>}
        {video && <VideoShower src={video} closeVideo={()=>setVideo("")}/>}
        {useMemo(()=><Passer switcher={props.switcher} imgView={1} nam="prasanth" />,[props.switcher])}
        </>
}

function VideoShower(props) {
    return <Video src={props.src} style={style.full_video} preLoad={true} className="absolute" delay={0} autoPlay={true} onEnded={props.closeVideo} />
}

const style = {
  full_video:{
            width:'100%',
            left:0,
            textAlign:'center',
          },
   top_center:{
     left:'18%',
     top:'7%',
     width:'50%'
   },
   swimming_pool:{
     bottom: '22%',
     width: '53%',
     left: 'initial',
     right: '-12%',
     top: 'initial'
   },
   instrution_board:{
     width:'95%',
     bottom:'0%',
     top:'initial',
     left:'0%'
   },
   man:{
     bottom: '24%',
     width: '13%',
     left: 'initial',
     right: '29%',
     top: 'initial'
   },
   boy_cyling:{
     width: '10%',
     left: '34%',
     top: '37%'
   },
   plate:{
     width: '23%',
     left: '8%',
     top: '52%'
   },
   clock:{
     width: '4%',
     left: '25.5%',
     top: '34%',
   },
   next_button:{
     width:'15%'
   }
}
