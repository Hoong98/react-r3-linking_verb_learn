import  { useState,useEffect,useContext,createRef } from 'react';
import { Link }           from "react-router-dom";
import PreLoaderContext   from '../context/preload_context';
import {Passer,Video,Audio}     from '../components/components';
import AnimationBox       from '../components/animation_box';
import LottieAnimation    from '../components/lottie';

//media
import {lotties} from '../config/lotties_config';

export default function UsingMoreLinkingVerbs(props){
  const preContext        = useContext(PreLoaderContext);
  const switcher          =[preContext.images.using_more_linking_verbs_first];
  const arr = [{},
    {
      switcher,
      prevent:true,
      next_time:24,
      video:preContext.videos.using_more_linking_verbs_01,
    },
    {
      switcher,
      prevent:true,
      next_time:13,
      video:preContext.videos.ice_cream,
    }
  ];

  return props.match.params.id == 3 ? <IntractionExample switcher={switcher}/> : <AnimationBox
                  key={props.match.params.id}
                  currentPath="/using_more_linking_verbs"
                  nextPath="/review2"
                  index={parseInt(props.match.params.id)}
                  maxIndex={arr.length}
                  data={arr[parseInt(props.match.params.id)]}
                  />
}
// {props.match.params.id == 3 && <IntractionExample switcher={switcher}/>}
//
function IntractionExample(props) {
  const preContext          = useContext(PreLoaderContext);
  const [answer,setAnswer]  = useState(0);

  return <>
      <Video src={preContext.videos.using_more_linking_verbs_loop} style={style.full_video} preLoad={true} className="absolute" delay={0} autoPlay={true} loop={true} />
      <Passer switcher={props.switcher} imgView={0}/>
      {answer   == 0 && <Question  onClick={setAnswer}/>}
      {answer   == 1 && <AnswerBox next_time={8} delay={2000} lottie={lotties.using_more_r} audio={preContext.audios.correct_answer_audio} nextPath="/review_2" onClick={setAnswer}/>}
      {answer   == 2 && <AnswerBox next_time={12} delay={0} lottie={lotties.using_more_w} audio={preContext.audios.wrong_answer_audio} nextPath="/review_2" onClick={setAnswer}/>}
  </>
}

function Question(props) {
  const preContext             = useContext(PreLoaderContext);
  const [selector,setSelector] = useState(false);

  function SelectorBox(){
    return <>
      <img src={preContext.images.que_img_0} style={{left: '6%',top:'42.5%'}} className="absolute vertical_center"/>
      <img src={preContext.images.que_img_3} style={{left:'31%',width:'8%',top:'43%'}} onClick={()=>props.onClick(2)} className="absolute vertical_center pointer"/>
      <img src={preContext.images.que_img_4} style={{left:'40%',width:'3%',top:'41.3%'}} className="absolute vertical_center"/>
      <img src={preContext.images.que_img_5} style={{left:'44%',width:'13%',top:'42.5%'}} onClick={()=>props.onClick(1)} className="absolute vertical_center pointer"/>
      <img src={preContext.images.que_img_2} style={{left:'59%',width:'31%',top:'43%'}} className="absolute vertical_center"/>
      <img src={preContext.images.que_img_1} style={{left:'59%',width:'27%',top:'52%'}} className="absolute vertical_center"/>
    </>
  }
  return selector ? <SelectorBox /> : <>
                                      <LottieAnimation data={lotties.using_more_child_que}/>
                                      <Audio onEnded={()=>setSelector(true)} src={preContext.audios.using_more_linking_child_que_audio} preLoad={true} delay={0} autoPlay={true} loop={true} />
                                      </>
}

function AnswerBox(props) {
  const [show,setShow]              = useState(false);
  const [nextAudio,setNextAudio]    = useState(false);
  const preContext                  = useContext(PreLoaderContext);
  return <>
           <LottieAnimation data={props.lottie}/>
           <Audio onEnded={()=>{setNextAudio(true);setShow(true)}} src={props.audio} preLoad={true} delay={props.delay} autoPlay={true} />
           {nextAudio && <Audio onEnded={()=>setShow(true)} src={preContext.audios.next_click_audio} preLoad={true} delay={0} autoPlay={true} loop={true} />}
           {show && <Link to={props.nextPath}><img src={preContext.images.next_button} className="center animated fadeIn bottom-right" style={style.next_button} /></Link>}
        </>
}


const style = {
  full_video:{
            width:'100%',
            left:0,
            textAlign:'center',
          },
  landing_img:{
            position:'absolute',
            width:'100%',
            left:0,
            top:0,
            textAlign:'center',
            zIndex:-1,
          },
   center_button:{
     width:'20%',
     bottom:'0%',
     zIndex:0
   },
   next_button:{
     width:'15%'
   }
}
