import  { useState,useEffect,useContext,createRef } from 'react';
import { Link } from "react-router-dom";
import PreLoaderContext from '../context/preload_context';
import AnimationBox from '../components/animation_box';



export default function MoreLinkingVerbs(props){
  const preContext        = useContext(PreLoaderContext);
  const arr = [{},
    {
      switcher:[preContext.images.more_linking_first,preContext.images.more_linking_second,preContext.images.more_linking_third],
      secondImgeTime:13,
      next_time:27,
      video:preContext.videos.more_linking_verbs_back,
    },
  ];

  return <AnimationBox currentPath="/more_linking_verbs"  singlePath={true} nextPath="/using_more_linking_verbs/1" data={arr[1]}/>;
}
